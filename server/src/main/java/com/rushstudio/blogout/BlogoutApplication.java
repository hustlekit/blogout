package com.rushstudio.blogout;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BlogoutApplication {

	public static void main(String[] args) {
		SpringApplication.run(BlogoutApplication.class, args);
	}

}
